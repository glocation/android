package com.globant.glocation.util;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/04/16
 */
public class Constants {

    private Constants() {
        // Nothing
    }

    public static final String TAG = "Glocation";

    public static class RequestCodes {
        public static final int LOGIN = 1000;
        public static final int SIGN_IN = 1001;
    }

    public static class Extras {
        public static final String ACCOUNT = "google_account";
    }
}
