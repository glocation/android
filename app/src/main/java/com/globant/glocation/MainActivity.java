package com.globant.glocation;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.globant.glocation.util.Constants;
import com.globant.glocation.view.BaseActivity;
import com.globant.glocation.view.LoginActivity;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, Constants.RequestCodes.LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.RequestCodes.LOGIN) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            } else {
                GoogleSignInAccount account = data.getParcelableExtra(Constants.Extras.ACCOUNT);
                Log.d(Constants.TAG, account.getDisplayName());
            }
        }
    }
}
