package com.globant.glocation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.globant.glocation.R;
import com.globant.glocation.util.Constants;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 15/04/16
 */
public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setResult(RESULT_CANCELED);

        setContentView(R.layout.activity_login);

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, Constants.RequestCodes.SIGN_IN);

        View signInButton = findViewById(R.id.btn_sign_in);
        if (signInButton != null) {
            signInButton.setOnClickListener(view -> {
                startActivityForResult(signInIntent, Constants.RequestCodes.SIGN_IN);
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RequestCodes.SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(Constants.TAG, "SignInResult: " + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount account = result.getSignInAccount();
            Intent intent = new Intent();
            intent.putExtra(Constants.Extras.ACCOUNT, account);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            // Signed out, show unauthenticated UI.
            finish();
        }
    }
}
